const recordingDiv = document.getElementById('recording');
const askDiv = document.getElementById('ask');

// When the page loads, hide the actual recording div until the microphone is accessed.
window.addEventListener('DOMContentLoaded', () => {
  console.log('loaded!');

  recordingDiv.style = 'display: none;';
  askDiv.style = 'display: none;';

  const canHover = !(matchMedia('(hover: none)').matches);
  if (canHover) {
    document.body.classList.add('can-hover');
  }

  // Some browsers don't support getUserMedia. Check that here.
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) askDiv.style = 'display: initial;';
  else {
    const oopsH2 = document.createElement('h2');
    const oopsP = document.createElement('p');

    oopsH2.innerHTML = 'Oops!';
    oopsP.innerHTML = 'Looks like your browser doesn\'t support microphone grabbing. Try from another browser.';

    document.body.appendChild(oopsH2);
    document.body.appendChild(oopsP);

    // The user cannot use this website from this browser.
    // There are no buttons for the user to click, therefore the code cannot advance.
  }

  // The fetch() API does not exist on IE. I check it here.
  if (!fetch) {
    const oopsH2 = document.createElement('h2');
    const oopsP = document.createElement('p');

    oopsH2.innerHTML = 'Oops!';
    oopsP.innerHTML = 'Looks like your browser doesn\'t support the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API#Browser_compatibility">fetch API</a>. Try from another browser.';

    document.body.appendChild(oopsH2);
    document.body.appendChild(oopsP);

    // The user cannot use this website from this browser.
    // There are no buttons for the user to click, therefore the code cannot advance.
  }
});

const prettyPleaseButton = document.getElementById('prettyPlease');
prettyPleaseButton.onclick = function () {
  // Request access to the user's microphone
  navigator.mediaDevices.getUserMedia({ audio: true, video: false })
    .then(handleSuccess);
};

// Get all the buttons I'll need later on
const share = document.getElementById('share');
const stopButton = document.getElementById('stop');
const record = document.getElementById('record');
const player = document.getElementById('player');
const warningText = document.getElementById('warning');

// Disable the stop button and share buttons until I'm ready to use them
stopButton.disabled = true;
share.disabled = true;

let chunks = [];
let blobToSend;

// Required for opus-media-recorder
const workerOptions = {
  OggOpusEncoderWasmPath: 'https://cdn.jsdelivr.net/npm/opus-media-recorder@latest/OggOpusEncoder.wasm',
  WebMOpusEncoderWasmPath: 'https://cdn.jsdelivr.net/npm/opus-media-recorder@latest/WebMOpusEncoder.wasm'
};
function handleSuccess(stream) {
  // Unhide the recording div and hide the ask div
  recordingDiv.style = 'display: initial;';
  askDiv.style = 'display: none;';

  // Set up to record a new stream of audio with the mimetype audio/webm
  const options = { mimeType: 'audio/webm' };
  const mediaRecorder = new MediaRecorder(stream, options, workerOptions);

  record.onclick = function () {
    mediaRecorder.start(); // Start recording
    console.log(mediaRecorder.state);
    console.log('recorder started');

    stopButton.disabled = false;
    record.disabled = true;
    warningText.style = 'display: none;'; // Remove the privacy policy warning text
  };

  stopButton.onclick = function () {
    mediaRecorder.stop();
    console.log(mediaRecorder.state);
    console.log('recorder stopped');
    // mediaRecorder.requestData(); // Not sure what this did?

    stopButton.disabled = true;
  };

  mediaRecorder.onstop = function () {
    console.log('data available after MediaRecorder.stop() called.');

    player.setAttribute('controls', '');
    player.controls = true;

    // Convert the chunks of audio we received into a Blob to send to the backend
    const blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' });
    blobToSend = blob;
    chunks = []; // Clear the chunks
    const audioURL = window.URL.createObjectURL(blob);
    player.src = audioURL;
    console.log('Recorder stopped! Audio loaded!', audioURL);

    share.disabled = false;
  };

  // This is triggered when the MediaRecorder is stopped.
  mediaRecorder.ondataavailable = function (e) {
    console.log('Data available!', e.data);
    // Push all of the audio data we received into the chunks array to be converted into a Blob later
    chunks.push(e.data);
  };
}

share.onclick = function () {
  // Disable the share button
  share.disabled = true;

  const recordingDiv = document.getElementById('recording');
  const p = document.createElement('p'); // Prepare the "Success!" or "Failed!" text
  recordingDiv.appendChild(p);

  // Prepare the copy button
  const copyButton = document.createElement('button');
  copyButton.innerHTML = 'Copy';

  console.log('share clicked');
  console.log('blob to send', blobToSend);

  // Prepare the data to send
  const formData = new FormData();

  formData.append('recording', blobToSend);

  // Upload to the backend!
  fetch('https://svr-back.akii.dev/upload', {
    method: 'POST',
    body: formData
  }).then(res => res.json()) // Convert to JSON
    .then(res => {
      console.log('res!', res); // We got a response!

      // If there was an error reported from the backend, display it to the user
      if (res.error) {
        p.innerHTML = `Error: ${res.message}`;
        // There was an error. Let them try again if they want.
        share.disabled = false;
      } else {
        // There was no error!
        p.innerHTML = `Success! Your URL is: <a href="${window.location.origin}/${res.path}">${window.location.origin}/${res.path}</a>`;
        recordingDiv.appendChild(copyButton);

        copyButton.onclick = () => {
          // Copy button writes the URL to the user's clipboard
          navigator.clipboard.writeText(`${window.location.origin}/${res.path}`);
          copyButton.innerHTML = 'Copied!';
        };
      }
    })
    .catch(e => {
      console.error(e);
      if (e.message.includes('NetworkError'))
        p.innerHTML = `${e}<br>(This means the backend service is down. Try again later.)`;
      else
        p.innerHTML = e;

      // There was an error. Let them try again if they want.
      share.disabled = false;
    });
};
