module.exports = {
  apps: [
    {
      name: 'svr-api',
      script: './index.js',
      watch: false,
      env: {
        'NODE_ENV': 'production',
        'PORT': 3000
      },
      env_debug: {
        'NODE_ENV': 'debug',
        'PORT': 3080
      }
    }
  ]
};
