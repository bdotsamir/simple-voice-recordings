console.log('Checking for an update from the backend...');
fetch('https://svr-back.akii.dev/update')
  .then(res => res.json())
  .then(res => {
    if(res.error) {
      return console.error('Backend sent an error while fetching update:', res.message);
    }

    if(!res.update) return console.log('No update.');

    console.log('There\'s an update from the dev! Display it!', res.message);

    const warningBanner = document.createElement('div');
    warningBanner.classList.add('banner');

    const warningP = document.createElement('p');
    warningP.innerHTML = `&#9888; ${res.message}`; // &#9888; = ⚠
    warningBanner.appendChild(warningP);
    
    const closeBtn = document.createElement('div');
    closeBtn.classList.add('closebtn');
    closeBtn.innerHTML = '&times;';
    warningBanner.appendChild(closeBtn); 

    // Make it not visible for a sec (put it above the page)
    warningBanner.style = 'transform: translateY(-100%);';
    // Then make it slide in
    setTimeout(() => warningBanner.style = 'transform: translateY(0%);', 750);

    document.body.appendChild(warningBanner);

    closeBtn.onclick = function() {
      // Slide up and off the page
      warningBanner.style = 'transform: translateY(-100%);';
    };

  }).catch(e => {
    console.error('Error fetching data from backend:', e);
  });